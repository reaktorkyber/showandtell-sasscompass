!SLIDE subsection
# Bruk i praksis

!SLIDE center
![Workflow](workflow3.png)

!SLIDE center
![Formats](NET211_tut_css_sas_output_form.jpg)

!SLIDE center
![Scout](scout.png)

!SLIDE center
![CompassApp](windows.jpg)

!SLIDE center
![Console](console2.png)
