!SLIDE subsection
# Compass

!SLIDE center
![compass](compass2.png)

!SLIDE bullets incremental
# Compass er et standardbibliotek
* Samling av nyttige Sass mixins og funksjoner

!SLIDE bullets incremental
# Inneholder blandt annet
* Typografi
* Layout/grids
* CSS3-mixins
* Hjelpefunksjoner

!SLIDE
# Utvidbart
## Du kan lett lage dine egne plugins
