!SLIDE subsection
# CSS

!SLIDE
# Den stygge andungen innen webutvikling

!SLIDE bullets incremental
# CSS skalerer ikke!
* Vedlikehold er alt for vanskelig
* Ikke gjenbrukbart
* Fullt av repetisjon
* Statisk

!SLIDE red
# Hvorfor har vi ikke tilsvarende verktøy som vi bruker til HTML?
