!SLIDE subsection
# Eksempler

!SLIDE
# Partials

!SLIDE center

![Structure](structure2.png)

!SLIDE
# CSS Reset

!SLIDE

    @@@ css
    @include global-reset();

!SLIDE
# Variabler

!SLIDE

    @@@ css
    // farger
    $body-text:   #bbb;
    $strong-text: darken($body-text, 40%);
    $muted-text:  lighten($body-text, 30%);

    // marginer
    $greenmargin: 12px;
    $redmargin:   $greenmargin * 2;
    $tightmargin: $greenmargin / 2;

!SLIDE
# Nøsting

!SLIDE

    @@@ css
    #main {
      width: 97%;

      p, div {
        font-size: 2em;
        a {
          font-weight: bold;
          &:hover { text-decoration: underline; }
        }
      }

      pre { font-size: 3em; }
    }

!SLIDE

    @@@ css
    #main {
      width: 97%; }
      #main p, #main div {
        font-size: 2em; }
        #main p a, #main div a {
          font-weight: bold; }
        #main p a:hover, #main div a:hover {
          text-decoration: underline; }
      #main pre {
        font-size: 3em; }

!SLIDE
# Arv

!SLIDE

    @@@ css
    .error {
      border: 1px #f00;
      background-color: #fdd;
    }

    .seriousError {
      @extend .error;
      border-width: 3px;
    }

!SLIDE
# Mixins

!SLIDE

    @@@css
    // bruk av mixins i sencha touch
    @include sencha-button-ui('Btntext', #ff8000);
    @include sencha-carousel-indicator-ui(..);
    @include sencha-tabbar-ui(..);
    @include sencha-toolbar(..);

!SLIDE

    @@@css
    // definere egne mixins
    @mixin horizontal-list($spacing: 10px) {
      li {
        float: left;
        margin-right: $spacing;
      }
    }

!SLIDE

    @@@css
    #header ul.nav {
      @include horizontal-list;
    }

    #footer ul.nav {
      @include horizontal-list(20px);
      margin-top: 1em;
    }

!SLIDE
# CSS3

!SLIDE

    @@@css
    .rounded {
      @include border-radius(4px, 4px);
    }

!SLIDE

    @@@css
    .rounded {
      -webkit-border-radius: 4px 4px;
      -moz-border-radius: 4px / 4px;
      -o-border-radius: 4px / 4px;
      -ms-border-radius: 4px / 4px;
      -khtml-border-radius: 4px / 4px;
      border-radius: 4px / 4px;
    }

!SLIDE

    @@@css
    h2 {
      @include opacity(50);
    }

!SLIDE

    @@@css
    h2 {
      opacity: 0.5;
      -moz-opacity: 0.5;
      -khtml-opacity: 0.5;
      -ms-filter: DXImageTransform osv;
      filter: alpha(opacity=50);
    }

!SLIDE
# Grids

!SLIDE

    @@@css
    $blueprint-grid-columns: 24;
    $blueprint-grid-width:   30px;
    $blueprint-grid-margin:  10px;

!SLIDE center

![PG](pggrid2.png)

!SLIDE center

![PG](pggridemacs.png)

!SLIDE
# Sprites

!SLIDE

    @@@css
    @import "icon/*.png";

    .actions {
      .new    { @include icon-sprite(new);    }
      .edit   { @include icon-sprite(edit);   }
      .save   { @include icon-sprite(save);   }
      .delete { @include icon-sprite(delete); }
    }
