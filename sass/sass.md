!SLIDE subsection
# Sass

!SLIDE red
# Syntactically
# Awesome
# Style
# Sheets

!SLIDE
# Sass er en utvidelse av CSS3
<!-- NOTE Sei noke om css superset osv -->

!SLIDE bullets incremental
# Sass gir deg:
* Kraftigere syntax
* Bedre abstraksjoner

<!-- NOTE organisering i "partials" og plugins/pakker -->

!SLIDE bullets incremental
# Utvidelser av CSS3:
* Nøsting
* Variabler
* Arv
* Mixins
* Partials

!SLIDE bullets incremental
# Utvidelser av CSS3:
* Funksjoner
* Kondisjoner
* Loops
* Kalkulasjoner
* Minifisering
